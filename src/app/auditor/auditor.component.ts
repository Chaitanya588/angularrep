﻿import { Component,ViewChild , OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { User } from '@/_models';
import { AuthenticationService,AuditorService } from '@/_services';

@Component({ templateUrl: 'auditor.component.html' })
export class AuditComponent implements OnInit {
    currentUser: User;
    users = [];    
    config: any;
    constructor(
        private authenticationService: AuthenticationService,       
        private auditorService: AuditorService
    ) {
        this.currentUser = this.authenticationService.currentUserValue;
        this.config = {
            itemsPerPage: 5,
            currentPage: 1,
            totalItems: this.users.length
          };
    }
    pageChanged(event){
        this.config.currentPage = event;
      }

    ngOnInit() {
        
        this.loadAllUsers();
    }

    private loadAllUsers() {
        this.auditorService.getAll()
            .pipe(first())
            .subscribe(users => this.users = users);
    }
}