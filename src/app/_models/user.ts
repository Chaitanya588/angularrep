﻿export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;   
    ipAddress: string;
    loginTime:string;
    logoutTime:string;
    role:string;
    token: string;
}