﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '@/_services';
import { User } from '@/_models';

import './_content/app.less';

@Component({ selector: 'app',templateUrl: 'app.component.html' })
export class AppComponent {
    currentUser: User;   
    public isAuditor : boolean =false;
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,        
    ) {
        this.authenticationService.currentUser.subscribe(x => this.changeUser(x));                                   
    }

    private changeUser(user: User): void {
        this.currentUser = user;               
        if(this.currentUser!=null ){
            if( this.currentUser.role == 'Auditor'){            
            this.isAuditor=true;
            console.log("updated"); 
        }               
    }
    }

    logout() {
        this.authenticationService.logout().subscribe(
            data => {                                    
                this.router.navigate(['/login']);
                this.isAuditor=false;
            },
            error => {
               console.log(error);
            });                      
    }
}