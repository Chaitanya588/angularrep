﻿import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '@/_models';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    @Output() getLoggedInUser: EventEmitter<any> = new EventEmitter();

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username, password, IpAddress) {
        return this.http.post<any>(`${config.apiUrl}/users/authenticate`, { username, password, IpAddress })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));                
                this.currentUserSubject.next(user);
                this.getLoggedInUser.emit(user);
                return user;
            }));
    }

    logout() {
        //Logout
        // remove user from local storage and set current user to null
        var curUser = this.currentUserSubject.value;                      
        return this.http.post(`${config.apiUrl}/users/logout`, curUser).pipe(map(
            res => {
                const response = res;                
                localStorage.removeItem('currentUser');
                this.currentUserSubject.next(null);
                return res;
            }
        ));        
    }
}